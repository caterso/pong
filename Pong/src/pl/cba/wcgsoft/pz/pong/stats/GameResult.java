package pl.cba.wcgsoft.pz.pong.stats;

import java.time.LocalDate;

public class GameResult {
	public enum MatchResult{
		WON, LOSS
	}
	private String playerName;
	private int playerResult, aiResult;
	private LocalDate gameDate;
	
	public GameResult() {}
	
	public GameResult(String playerName, int playerResult, int aiResult, LocalDate gameDate) {
		super();
		this.playerName = playerName;
		this.playerResult = playerResult;
		this.aiResult = aiResult;
		this.gameDate = gameDate;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getPlayerResult() {
		return playerResult;
	}

	public void setPlayerResult(int playerResult) {
		this.playerResult = playerResult;
	}

	public int getAiResult() {
		return aiResult;
	}

	public void setAiResult(int aiResult) {
		this.aiResult = aiResult;
	}

	public LocalDate getGameDate() {
		return gameDate;
	}

	public void setGameDate(LocalDate gameDate) {
		this.gameDate = gameDate;
	}
	
	public void incAiResult(){
		aiResult++;
	}
	
	public void incPlayerResult(){
		playerResult++;
	}
	
	public MatchResult getSummaryStatus(){
		if(playerResult==10)
			return MatchResult.WON;
		else
			return MatchResult.LOSS;
	}
	
	
	
	
	
	
	
}
