package pl.cba.wcgsoft.pz.pong.stage;

public enum CollisionSide {
 LEFT, RIGHT, TOP, BOTTOM
}
