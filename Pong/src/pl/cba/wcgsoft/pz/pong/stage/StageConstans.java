package pl.cba.wcgsoft.pz.pong.stage;

import java.awt.Font;

public final class StageConstans {
	public static final int PADDLE_MARGIN = 10;
	public static int BALL_BASE_DX = 3;
	public static int BALL_BASE_DY = 3;
	public static final String HIT_MSG = "HIT";
	public static final String LOSS_MSG = "AI won";
	public static final String WON_MSG = "You won!";
	public static final String PAUSE_MSG = "Pause";
	public static final String CONTINUE_MSG = "Press space continue...";
	public static final Font MSG_FONT = new Font("Arial Black", Font.BOLD, 20);
}
