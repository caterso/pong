package pl.cba.wcgsoft.pz.pong.stage;

public enum SurfaceImpactVector {
	HORIZONTAL(1,0), VERTICAL(0,1);
	private int vector[];
	private SurfaceImpactVector(int dx, int dy) {
		vector = new int[2];
		vector[0]=dx;
		vector[1]=dy;
	}
	
	public int[] getVector() {
		return vector;
	}
	
	
}
