package pl.cba.wcgsoft.pz.pong.stage;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints.Key;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.print.attribute.ResolutionSyntax;
import javax.print.attribute.standard.RequestingUserName;
import javax.swing.JPanel;

import pl.cba.wcgsoft.pz.pong.actor.Actor;
import pl.cba.wcgsoft.pz.pong.actor.Ball;
import pl.cba.wcgsoft.pz.pong.actor.Paddle;
import pl.cba.wcgsoft.pz.pong.state.GameState;
import pl.cba.wcgsoft.pz.pong.state.GameStateListener;
import pl.cba.wcgsoft.pz.pong.stats.GameResult;

public class Stage extends JPanel implements KeyListener{
	private Logger logger;
	private Thread gameThread;
	private GameState state;
	private boolean gameOver;
	private GameResult result;
	private int stageWidth, stageHeight;
	private int[] playerMoveVector;
	private Paddle player, ai;
	private Paddle[] paddles;
	private Ball ball;
	private Random rand;
	private int counter;
	private ArrayList<GameStateListener> listeners;
	private boolean waitForBall;
	public Stage(int stageWidth, int stageHeight, ArrayList<Actor> actors) {
		this.stageHeight=stageHeight;
		this.stageWidth=stageHeight;
		loadActors(actors);
		setDoubleBuffered(true);
		rand = new Random(System.currentTimeMillis());
		result = new GameResult();
		logger = Logger.getLogger(Stage.class.getName());
		counter = 3;
	}
	private void loadActors(ArrayList<Actor> actors){
		paddles = new Paddle[2];
		for (Actor actor : actors) {
			if(actor instanceof Paddle){				
				if(((Paddle) actor).isPlayer()){
					player=(Paddle) actor;
					paddles[0]=player;
				}
				else{
					ai = (Paddle) actor;
					paddles[1]=ai;
				}
			}
			else
				ball=(Ball) actor;			
		}
	}
	
	public void addGameStateChangeListener(GameStateListener listener){
		if(listeners==null)
			listeners=new ArrayList<>();
		listeners.add(listener);
	}
	
	public void removeGameStateChangeListener(GameStateListener listener){
		if(listeners==null||listeners.isEmpty())
			return;
		listeners.remove(listener);
	}
	private void fireGameStateChange(){
		if(listeners!=null)
			for (GameStateListener gameStateListener : listeners) {
				gameStateListener.gameStateChange(state);
			}
	}
	
	public void startGame(){
		gameOver = false;		
		if(gameThread==null){
			gameThread=new Thread(new Runnable() {				
				@Override
				public void run() {
					resetBallPostioin();
					countDown();					
					state=GameState.PLAY;
					while(!gameOver){
						detectCollisions();
						if(state==GameState.HIT)
							onHit();
						else if(state==GameState.PAUSE){
							repaint();
							while(state!=GameState.PLAY&&state!=GameState.ABANDON_GAME){
								try {
									Thread.sleep(100);
								} catch (InterruptedException e) {
									logger.log(Level.FINEST, "Exception on game thread sleep while waiting for game state change", e);
								}
							}
						}
						if(state==GameState.ABANDON_GAME)
							break;
						player.move();
						ai.move();
						ball.move();
						repaint();						
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							logger.log(Level.FINEST, "Exception on game thread sleep", e);
						}
					}
					
					onGameOver();					
				}
			});
			gameThread.start();
		}
	}	 
	
	private void countDown(){
		counter = 3;
		state = GameState.COUNTDOWN;
		do{
			repaint();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}while(counter-->0);
		
	}
	
	private void detectCollisions(){	
		for (Paddle paddle : paddles) {
			
			if(paddle.checkIntersection(ball)&&(ball.getLastCollisionActor()==null||ball.getLastCollisionActor()!=paddle)){					
				if(ball.getLastCollisionActor()==null){
					ball.setDx(ball.getDx()+2);
					ball.setDy(ball.getDy()+2);
				}
				
				CollisionSide side = paddle.getCollisionSide(ball);
				ball.onCollision(SurfaceImpactVector.HORIZONTAL, side);
				if(side!=CollisionSide.TOP)
					if(!ball.isPaddleAccelerated())
						ball.setPaddleAcceleration(paddle.getDx());
				    ball.setLastCollisionActor(paddle); 				
			}
			
			
		}
		if(ball.getX()<=0)
			ball.onCollision(SurfaceImpactVector.VERTICAL, CollisionSide.LEFT);
		else if (ball.getX()+ball.getWidth()>=stageWidth)
			ball.onCollision(SurfaceImpactVector.VERTICAL, CollisionSide.RIGHT);
		
		for (Actor actor : paddles) {
			if(actor.getX()<=0)
				actor.onCollision(SurfaceImpactVector.VERTICAL, CollisionSide.LEFT);
			else if (actor.getX()+actor.getWidth()>=stageWidth)
				actor.onCollision(SurfaceImpactVector.VERTICAL, CollisionSide.RIGHT);
			
			if((ball.getLastCollisionActor()==null||((Paddle)ball.getLastCollisionActor()).isPlayer())&&!((Paddle) actor).isPlayer()){
				
		    	int x = 0;
		    	if(ball.getX()<actor.getX()+2)
		    		x=-1;
		    	else if(ball.getX()+ball.getWidth()/2>actor.getX()+actor.getWidth()-1)
		    		x=1;
		    	else x=0;
		    	actor.getMoveVector()[0]=x;
		    
		}
		}
		if(ball.getY()+ball.getHeight()>=stageHeight+ball.getHeight()/2){
			state=GameState.HIT;
			result.incAiResult();
		}			
		else if(ball.getY()<=-(ball.getHeight()/2)){
			state=GameState.HIT;
			result.incPlayerResult();
		}
		if(result.getAiResult()==10 || result.getPlayerResult() == 10){
			gameOver = true;
			state = GameState.GAME_OVER;
		}
			
	}
	
	private void onHit(){
		resetBallPostioin();
		repaint();		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			logger.log(Level.FINEST, "Exception on game thread sleep, on hit", e);
		}
		state = GameState.PLAY;
	}
	private void resetBallPostioin(){
		ball.setX(5+rand.nextInt(stageWidth-2*StageConstans.PADDLE_MARGIN));								
		int[] ballVector = {rand.nextInt()%2==0?-1:1,rand.nextInt()%2==0?-1:1};
		ball.setY(stageHeight/2-rand.nextInt(40)*ballVector[1]);	
		ball.setMoveVector(ballVector);
		ball.setDx(StageConstans.BALL_BASE_DX);
		ball.setDy(StageConstans.BALL_BASE_DY);
		ball.resetAcceleration();
		ball.setLastCollisionActor(null);
	}
	
	private void onGameOver(){
		repaint();
		if(state == GameState.GAME_OVER)
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				logger.log(Level.FINEST, "Exception on game thread sleep. Game over", e);
			}
		fireGameStateChange();
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D gd = (Graphics2D)g;
		gd.setFont(StageConstans.MSG_FONT);
		gd.setColor(Color.BLACK);
		gd.fillRect(0, 0, stageWidth, stageHeight);
		gd.setColor(Color.WHITE);	
		FontMetrics fontMetrics = gd.getFontMetrics();
		if(state!= GameState.GAME_OVER){	
			
			gd.fillOval(ball.getX(), ball.getY(), ball.getWidth(), ball.getHeight());		
			for (Actor actor : paddles) {
				gd.fillRect(actor.getX(), actor.getY(), actor.getWidth(), actor.getHeight());
			}		
			Graphics2D g2d = (Graphics2D) g.create();	        
	        Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
	        g2d.setStroke(dashed);
	        g2d.drawLine(0, stageHeight/2, stageWidth, stageHeight/2);	        
	        g2d.dispose();	        
	        
	        gd.drawString(String.valueOf(result.getAiResult()), StageConstans.PADDLE_MARGIN, stageHeight/2-fontMetrics.getHeight());
	        gd.drawString(String.valueOf(result.getPlayerResult()), StageConstans.PADDLE_MARGIN, stageHeight/2+fontMetrics.getHeight());
	        if(state==GameState.HIT){	        	
	        	gd.drawString(StageConstans.HIT_MSG, stageWidth/2-fontMetrics.stringWidth(StageConstans.HIT_MSG)/2, stageHeight/2-fontMetrics.getHeight()/2);
	        }
	        else if(state == GameState.COUNTDOWN)
				gd.drawString(String.valueOf(counter), stageWidth/2-fontMetrics.stringWidth(String.valueOf(counter))/2, stageHeight/2-fontMetrics.getHeight()/2);
		}
		
		else{
			String msg = result.getAiResult()==10?StageConstans.LOSS_MSG:StageConstans.WON_MSG;
			gd.drawString(msg, stageWidth/2-fontMetrics.stringWidth(msg)/2, stageHeight/2-fontMetrics.getHeight()/2);			
		}
		if(state==GameState.PAUSE){
			gd.drawString(StageConstans.PAUSE_MSG, stageWidth/2-fontMetrics.stringWidth(StageConstans.PAUSE_MSG)/2, stageHeight/2-fontMetrics.getHeight()/2);
			gd.drawString(StageConstans.CONTINUE_MSG, stageWidth/2-fontMetrics.stringWidth(StageConstans.CONTINUE_MSG)/2, stageHeight/2+fontMetrics.getHeight());
		}
		else if(state==GameState.ABANDON_GAME){
			gd.fillRect(0, 0, stageWidth, stageHeight);
		}
        
        
	}
	
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void keyPressed(KeyEvent e) {
		playerMoveVector = player.getMoveVector();
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			playerMoveVector[0]=-1;			
			break;
		case KeyEvent.VK_RIGHT:
			playerMoveVector[0]=(1);
			break;
		case KeyEvent.VK_ESCAPE:
			if(state==GameState.PLAY)
				state = GameState.PAUSE;
			else if(state==GameState.PAUSE)
				state = GameState.ABANDON_GAME;
			break;
		case KeyEvent.VK_SPACE:
			if(state==GameState.PAUSE)
				state = GameState.PLAY;			
		}
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		playerMoveVector = player.getMoveVector();
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			if(!(playerMoveVector[0]>0))
				playerMoveVector[0]=0;
			break;
		case KeyEvent.VK_RIGHT:
			if(!(playerMoveVector[0]<0))
			playerMoveVector[0]=0;
			break;
		}
	}
}
