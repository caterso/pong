package pl.cba.wcgsoft.pz.pong.state;

public interface GameStateListener {
	public void gameStateChange(GameState newGameState);
}
