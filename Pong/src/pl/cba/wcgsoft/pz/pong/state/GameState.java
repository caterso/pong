package pl.cba.wcgsoft.pz.pong.state;

public enum GameState {
	COUNTDOWN, MENU, PAUSE, PLAY, HIT, GAME_OVER, ABANDON_GAME;

}
