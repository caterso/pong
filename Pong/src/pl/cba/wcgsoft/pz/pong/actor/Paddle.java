package pl.cba.wcgsoft.pz.pong.actor;

import java.util.Random;

import pl.cba.wcgsoft.pz.pong.stage.CollisionSide;
import pl.cba.wcgsoft.pz.pong.stage.SurfaceImpactVector;

public class Paddle extends Actor {
	private boolean isPlayer;
	private PaddlePosition position;
	private int frameMovementLimit, aiMistake;
	private Random rand;
	
	public Paddle(int x, int y) {
		super(x, y);
		width=90;
		height=10;
		dy=0;
		dx=6;
		position = PaddlePosition.TOP;
		frameMovementLimit = 0;
		rand = new Random(System.currentTimeMillis());
		aiMistake = rand.nextInt(2);
	}
	@Override
	public void move() {
		if(!isPlayer){
			if(frameMovementLimit++>=(6-aiMistake)){				
				frameMovementLimit=0;
			}
			else{
				super.move();
				aiMistake = rand.nextInt(4);
			}
		}
		else super.move();
	}
	
	@Override
	protected void transfromMoveVector(int[] actorMoveVector, int[] surfaceImpactVector) {}
	
	@Override
	public void onCollision(SurfaceImpactVector impactVector, CollisionSide side) {
		if(side==CollisionSide.LEFT&&moveVector[0]<0)
			moveVector[0]=0;
		else if(side==CollisionSide.RIGHT&&moveVector[0]>0)
			moveVector[0]=0;	
	}
	
	public void setPlayer(boolean isPlayer) {
		this.isPlayer = isPlayer;
		width=90;
	}
	
	public boolean isPlayer() {
		return isPlayer;
	}
	
	public PaddlePosition getPosition() {
		return position;
	}
	
	public void setPosition(PaddlePosition position) {
		this.position = position;
	}
	
	@Override
	public CollisionSide getCollisionSide(Actor actor) {
		if(actor.x+actor.getWidth()/2>x&&actor.x+actor.width/2<x+getWidth())
			return CollisionSide.TOP;
		if(actor.x+actor.getWidth()/2>x)
		  return CollisionSide.LEFT;
		else return CollisionSide.RIGHT;
	}
	
	
}
