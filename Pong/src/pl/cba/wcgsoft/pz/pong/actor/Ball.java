package pl.cba.wcgsoft.pz.pong.actor;

import java.util.Random;

import pl.cba.wcgsoft.pz.pong.stage.CollisionSide;
import pl.cba.wcgsoft.pz.pong.stage.SurfaceImpactVector;

public class Ball extends Actor {
	private Random random;
	private int paddleDx;
	boolean down = false;
	private Actor lastCollisionActor;
	private boolean paddleAccelerated;
	public Ball() {
		this(0, 0);
	}
	public Ball(int x, int y) {
		super(x,y,5,5);
		height=width=15;
		random = new Random();
		random.setSeed(System.currentTimeMillis());
		paddleAccelerated = false;
	}
	
	public void setPaddleAcceleration(int dy){
		paddleAccelerated=true;
		paddleDx=dy;
		this.dy+=paddleDx/2;
		this.dx-=paddleDx/2;		
	}
	
	public void resetAcceleration(){
		paddleAccelerated=false;
		paddleDx=0;
		
	}
	
	public boolean isPaddleAccelerated() {
		return paddleAccelerated;
	}

	protected void transfromMoveVector(int[] actorMoveVector, int[] surfaceImpactVector){
		for(int i=0; i<actorMoveVector.length&&i<surfaceImpactVector.length;i++){
			actorMoveVector[i]=(surfaceImpactVector[i]==0?actorMoveVector[i]*-1:actorMoveVector[i]);
		}
	}

	@Override
	public void onCollision(SurfaceImpactVector impactVector, CollisionSide side) {		
		if(impactVector==SurfaceImpactVector.HORIZONTAL&&side!=CollisionSide.TOP){
			moveVector[0]*=-1;
			moveVector[1]*=-1;			
		}
		
		else{ 
			if(!(side==CollisionSide.LEFT&&moveVector[0]>0) && !(side==CollisionSide.RIGHT&&moveVector[0]<0) )
				transfromMoveVector(moveVector, impactVector.getVector());
			if(impactVector==SurfaceImpactVector.HORIZONTAL&&paddleAccelerated){
				paddleAccelerated=false;
				this.dy-=paddleDx/2;
				this.dx+=paddleDx/2;				
			}
		}
			
	}
	
	public Actor getLastCollisionActor() {
		return lastCollisionActor;
	}

	public void setLastCollisionActor(Actor lastCollisionActor) {
		this.lastCollisionActor = lastCollisionActor;
	}

}
