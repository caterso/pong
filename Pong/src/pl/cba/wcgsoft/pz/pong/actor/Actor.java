package pl.cba.wcgsoft.pz.pong.actor;

import java.awt.Rectangle;

import pl.cba.wcgsoft.pz.pong.stage.CollisionSide;
import pl.cba.wcgsoft.pz.pong.stage.SurfaceImpactVector;

public abstract class Actor {
	protected int x, y;
	protected int[] moveVector;
	protected int dx, dy;
	protected int width, height;
	public Actor() {
		moveVector = new int[2];
		x=y=0;
		dx=dy=2;		
	}
	
	public Actor(int x, int y){
		moveVector = new int[2];
		this.x=x;
		this.y=y;
	}
	
	public Actor(int x, int y, int dx, int dy){
		moveVector = new int[2];
		this.x=x;
		this.y=y;
		this.dx=dx;
		this.dy=dy;
	}
	

	public void move() {		
		x=x+dx*moveVector[0];
		y=y+dy*moveVector[1];
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int[] getMoveVector() {
		return moveVector;
	}

	public void setMoveVector(int[] moveVector) {
		this.moveVector = moveVector;
	}

	public int getDx() {
		return dx;
	}

	public void setDx(int dx) {
		this.dx = dx;
	}

	public int getDy() {
		return dy;
	}

	public void setDy(int dy) {
		this.dy = dy;
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	protected abstract void transfromMoveVector(int[] actorMoveVector, int[] surfaceImpactVector);
	
	public abstract void onCollision(SurfaceImpactVector impactVector, CollisionSide side);
	
	public boolean checkIntersection(Actor actor){		
		return new Rectangle(x,y,width,height).intersects(new Rectangle(actor.getX(),actor.getY(),actor.getWidth(),actor.getHeight()));
	}
	
	public CollisionSide getCollisionSide(Actor actor){
		if(actor.x+actor.getWidth()/2>x&&actor.x+actor.width/2<x+getWidth())
			return CollisionSide.TOP;
		if(actor.x+actor.getWidth()/2>x)
		  return CollisionSide.LEFT;
		else return CollisionSide.RIGHT;
	}
	
	
			
	

}
