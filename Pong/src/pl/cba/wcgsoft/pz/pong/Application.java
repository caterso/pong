package pl.cba.wcgsoft.pz.pong;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import pl.cba.wcgsoft.pz.pong.actor.Actor;
import pl.cba.wcgsoft.pz.pong.actor.Ball;
import pl.cba.wcgsoft.pz.pong.actor.Paddle;
import pl.cba.wcgsoft.pz.pong.actor.PaddlePosition;
import pl.cba.wcgsoft.pz.pong.stage.Stage;
import pl.cba.wcgsoft.pz.pong.stage.StageConstans;
import pl.cba.wcgsoft.pz.pong.state.GameState;
import pl.cba.wcgsoft.pz.pong.state.GameStateListener;

public final class Application extends JApplet implements GameStateListener, ActionListener{
	private int WIDTH, HEIGHT;	
	private JPanel menuPanel, instructionsPanel;
	private JButton play, help, backI;
	private Stage stage;
	private Application app;
	private StringBuilder msg;
	private Random rand;
	private Logger logger;
	@Override
	public void init() {		
		super.init();		
		msg = new StringBuilder();
		logger = Logger.getLogger(Application.class.getName());
		rand = new Random(System.currentTimeMillis());
		//file is very short, there is no sense to run this in SwingWorker
		String line;
		try(BufferedReader input = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("htp.txt")))) {
			while ((line = input.readLine()) != null) {
			  msg.append(line);
			  msg.append("\n");
			}
		} catch (IOException e1) {
			logger.log(Level.FINEST, "Trying to read htp.txt file", e1);
		}
		
		WIDTH = getWidth();
		HEIGHT = getHeight();
		setFocusable(true);
		app = this;
		try {
			buildUI();
		} catch (InvocationTargetException e) {
			logger.log(Level.FINEST, "Exception on build UI", e);
		} catch (InterruptedException e) {
			logger.log(Level.FINEST, "Exception on build UI", e);
		}
	}

	@Override
	public void gameStateChange(GameState newGameState) {
		switch (newGameState) {
		case GAME_OVER:
		case ABANDON_GAME:					
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					removeKeyListener(stage);
					stage.removeGameStateChangeListener(app);
					remove(stage);
					stage = null;
					add(menuPanel);
					repaint();
				}
			});	
			break;
		default:
			break;
		}
		
	}	
	
	private void buildUI() throws InvocationTargetException, InterruptedException{
		SwingUtilities.invokeAndWait(new Runnable() {
			
			@Override
			public void run() {				
				try {
					UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
					logger.log(Level.FINEST, "Exception on change L&F", e);
				}
				
				play = new JButton("Play");
				play.addActionListener(app);
				
				help = new JButton("How to play");
				help.addActionListener(app);			
				
				backI = new JButton("Back");
				backI.addActionListener(app);	
				
				menuPanel = new JPanel(new GridLayout(10, 1));
				JLabel title = new JLabel("The Pong",SwingConstants.CENTER);
				title.setFont(StageConstans.MSG_FONT);
				menuPanel.add(title);
				menuPanel.add(new JLabel());
				menuPanel.add(play);			
				menuPanel.add(help);
				for (int i = 0; i < 5; i++)
					menuPanel.add(new JLabel(""));					
				
				menuPanel.add(new JLabel("Created by Bartosz Chrostek"));
				instructionsPanel = new JPanel(new BorderLayout());
				JTextArea textArea = new JTextArea(msg.toString());
				textArea.setEditable(false);
				instructionsPanel.add(new JScrollPane(textArea));
				instructionsPanel.add(backI, BorderLayout.SOUTH);				
				
				add(menuPanel);
				
			}
		});
		
	}	
	
	private Stage buildStage(){
		
		ArrayList<Actor> actors = new ArrayList<>(3);
		actors.add(new Paddle(0, StageConstans.PADDLE_MARGIN));
		
		Paddle player = new Paddle(25, HEIGHT- StageConstans.PADDLE_MARGIN*2);
		player.setPlayer(true);
		player.setPosition(PaddlePosition.BOTTOM);
		actors.add(player);
		
		Ball ball = new Ball();	
		actors.add(ball);
		
		return new Stage(WIDTH, HEIGHT, actors);	
	}	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==backI){
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					remove(instructionsPanel);
					add(menuPanel);
					validate();
					repaint();					
				}
			});
		
		}
		
		else if(e.getSource()==help){	
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					remove(menuPanel);
					add(instructionsPanel);	
					validate();
					repaint();						
				}
			});
			
		}	
		else if(e.getSource()==play){
			stage = buildStage();
			stage.addGameStateChangeListener(this);
			addKeyListener(stage);
			remove(menuPanel);
			add(stage);
			validate();
			stage.startGame();					
			
		}
		
	}
	
}
